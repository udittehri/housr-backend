const Product = require('./../models/product')

class ProductService {

    getProductList(request = {}) {
        return Product.find({});
    }
    addNewProduct(data) {
        return new Product(data).save();
    }
    async updateProduct(criteria, details) { 
        return await Product.findOneAndUpdate(criteria, details, { new: true });
    }

}

module.exports = new ProductService();