const express = require('express');
const router = express.Router();
const ProductController = require('./../../controllers/products')

router.post('/product',ProductController.addProduct)
router.get('/product',ProductController.getAllProducts)
router.patch('/product',ProductController.editProducts)

module.exports = router;