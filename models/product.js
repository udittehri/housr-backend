const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name: {
        type: Object,
        required: true
    },
    description: {
        type: Object,
        required: true
    },
    type: {
        type: Number,
        enum: [1, 2],
        default: 1  // 1-> Service, 2-> Product
    },
    frequency: {
        type: Number,
        enum: [1, 2, 3],
        default: 3  // 1-> Monthy, 2-> Daily, 3 - Not Defined
    },
    basePrice: {
        type: Number,
        required: true,
    },
    sellPrice: {
        type: Number,
        required: true,
    },
    subsPrice: {
        type: Number,
        required: true,
    },
    subsBasePrice: {
        type: Number,
        required: true,
    },
    gst: {
        type: Number,
        required: true,
    },
    status: {
        type: Number,
        enum: [1, 2],
        default: 1  // 1-> Active, 2-> InActive
    }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

module.exports = mongoose.model('Product', schema);
