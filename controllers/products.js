const ProductService = require('./../services/projects')

class ProductController {

    async getAllProducts(req, res) {

        try {

            let products = await ProductService.getProductList();
            console.log(products);

            return res.status(200).send(products);

        } catch (e) {
            return res.status(500).send({ e });
        }
    }
    async addProduct(req, res) {

        try {
            let request = { ...req.body }
            if (!request.name) throw { message: 'Name is required' };
            if (!request.description) throw { message: 'Description is required' };
            if (!request.type) throw { message: 'Type is required' };
            if (!request.frequency) throw { message: 'Frequency is required' };
            if (!request.basePrice) throw { message: 'Base Price is required' };
            if (!request.sellPrice) throw { message: 'Selling Price is required' };
            if (!request.subsPrice) throw { message: 'Subscription Price is required' };
            if (!request.subsBasePrice) throw { message: 'Subscription Base Price is required' };
            if (!request.gst) throw { message: 'GST is required' };

            let r = ProductService.addNewProduct(request);
            // console.log(r, 'lllll');
            return res.status(200).send(r);

        } catch (e) {
            console.log(e);

            return res.status(500).send({ e });
        }
    }

    async editProducts(req, res) {

        try {
            let request = { ...req.body };
            let r = ProductService.updateProduct({ _id: request.id }, request.changes)
            return res.status(200).send();

        } catch (e) {
            return res.status(500).send({ e });
        }
    }

}

module.exports = new ProductController();